package org.pan.onecard.common;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.persist.jpa.JpaPersistModule;
import lombok.Cleanup;
import org.skife.config.ConfigurationObjectFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;
import java.util.concurrent.ThreadFactory;

/**
 * @author panmingzhi
 */
public class MyGuiceModule implements Module {
    private static ConfigBean getConfig() {
        try {
            if(Files.notExists(ConfigBean.path)){
                Files.createFile(ConfigBean.path);
            }

            Properties props = new Properties();
            @Cleanup InputStream inStream = Files.newInputStream(ConfigBean.path);
            props.load(inStream);
            ConfigurationObjectFactory factory = new ConfigurationObjectFactory(props);
            return factory.build(ConfigBean.class);
        } catch (IOException e) {
            throw new RuntimeException("加载配置文件异常",e);
        }
    }

    @Override
    public void configure(Binder binder) {
        binder.install(new JpaPersistModule("derby"));
        binder.bind(ThreadFactory.class).toInstance(new ThreadFactoryBuilder().build());
        binder.bind(ConfigBean.class).toProvider(MyGuiceModule::getConfig);
    }
}
