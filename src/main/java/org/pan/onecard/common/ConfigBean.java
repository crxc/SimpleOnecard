package org.pan.onecard.common;

import org.skife.config.Config;
import org.skife.config.Default;

import java.nio.file.Path;
import java.nio.file.Paths;

public interface ConfigBean {
    Path path = Paths.get("application.properties");

    @Config("login.remember")
    @Default("false")
    Boolean remember();

    @Config("login.username")
    @Default("admin")
    String username();

    @Config("login.password")
    @Default("123456")
    String password();
}
