package org.pan.onecard.common;

import com.google.common.eventbus.EventBus;

public class EventBusHelper extends EventBus {
    private static EventBus eventBus = new EventBus("application");

    public static EventBus getDefault() {
        return eventBus;
    }
}
