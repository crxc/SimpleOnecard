package org.pan.onecard.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ControllerFx {
    /**
     * fxml 地址
     *
     * @return
     */
    String fxml() default "";
}
