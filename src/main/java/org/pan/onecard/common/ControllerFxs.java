package org.pan.onecard.common;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import org.pan.onecard.Main;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author panmingzhi
 */
@Slf4j
public class ControllerFxs {
    private static Injector injector;

    public static void init(Stage primaryStage) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            injector = Guice.createInjector(new MyGuiceModule(), binder -> binder.bind(Stage.class).toInstance(primaryStage));
        } finally {
            log.debug("{} use time {} ms", "init", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        }
    }

    public static Parent load(String controllerClass) {
        log.debug("load controller {}", controllerClass);
        try {
            return load(Class.forName(controllerClass));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("load controller is error", e);
        }
    }

    public static Parent load(Class<?> controllerClass) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            String fxml = controllerClass.getAnnotation(ControllerFx.class).fxml();
            Preconditions.checkArgument(!Strings.isNullOrEmpty(fxml), "fxml value is empty");
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(Charsets.UTF_8);
                fxmlLoader.setControllerFactory(injector::getInstance);
                fxmlLoader.setLocation(Main.class.getResource(fxml));
                return fxmlLoader.load(Main.class.getResourceAsStream(fxml));
            } catch (IOException e) {
                throw new RuntimeException("load fxml error", e);
            }
        } finally {
            log.debug("load {} use time {} ms", controllerClass, stopwatch.elapsed(TimeUnit.MILLISECONDS));
        }
    }
}
