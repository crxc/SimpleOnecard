package org.pan.onecard.controllers;

import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import org.pan.onecard.bean.SystemMenu;
import org.pan.onecard.common.ControllerFx;
import org.pan.onecard.common.ControllerFxs;
import org.pan.onecard.common.EventBusHelper;
import org.pan.onecard.component.DragListener;
import org.pan.onecard.component.MessageDialog;
import org.pan.onecard.component.SystemMenuTreeItem;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * @author panmingzhi
 */
@Slf4j
@ControllerFx(fxml = "/fxml/Layout.fxml")
public class Layout implements Initializable {
    public TabPane tab;
    public TreeView<SystemMenu> tree;
    public HBox top;
    @Inject
    private Stage stage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new DragListener(stage).enableDrag(top);
        EventBusHelper.getDefault().register(this);
        SystemMenuTreeItem.loadDataFromJson(tree, "/menu.json");

        Parent load = ControllerFxs.load(DashBoard.class);
        Tab tabItem = new Tab("首页");
        tabItem.setContent(load);
        tabItem.setClosable(false);
        tab.getTabs().add(tabItem);
        tab.getSelectionModel().select(tabItem);
    }

    @Subscribe
    public void selectTreeItem(TreeItem<SystemMenu> treeItem) {
        log.debug("select tree item {}", treeItem.getValue());
        Optional.of(treeItem).filter(TreeItem::isLeaf).map(TreeItem::getValue).filter(f -> !Strings.isNullOrEmpty(f.getController())).ifPresent(p -> {
            Optional<Tab> any = tab.getTabs().parallelStream().filter(f -> p == f.getUserData()).findAny();
            if (any.isPresent()) {
                tab.getSelectionModel().select(any.get());
                return;
            }
            Parent load = ControllerFxs.load(p.getController());
            load.getStylesheets().add("/css/Layout.css");
            load.getStyleClass().add("tab-content");
            Tab tabItem = new Tab(p.getName());
            tabItem.setUserData(p);
            tabItem.setContent(load);
            tabItem.setClosable(true);
            tab.getTabs().add(tabItem);
            tab.getSelectionModel().select(tabItem);
        });
    }

    public void modifyPassword(ActionEvent actionEvent) {

    }

    public void logout(ActionEvent actionEvent) {
        if (MessageDialog.confirm(stage,"确认注销吗?")) {
            Parent load = ControllerFxs.load(Login.class);
            stage.setScene(new Scene(load));
            stage.centerOnScreen();
        }
    }

    public void max(MouseEvent mouseEvent) {
        stage.setMaximized(!stage.maximizedProperty().get());
    }

    public void close(MouseEvent mouseEvent) {
        if (MessageDialog.confirm(stage,"确认关闭吗?")) {
            System.exit(1);
        }
    }

    public void min(MouseEvent mouseEvent) {
        stage.setIconified(true);
    }
}
