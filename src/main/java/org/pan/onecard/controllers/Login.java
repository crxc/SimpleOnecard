package org.pan.onecard.controllers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.pan.onecard.common.ConfigBean;
import org.pan.onecard.common.ControllerFx;
import org.pan.onecard.common.ControllerFxs;
import org.pan.onecard.component.DragListener;
import org.pan.onecard.component.MessageDialog;
import org.pan.onecard.component.Validor;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadFactory;

@Slf4j
@ControllerFx(fxml = "/fxml/Login.fxml")
public class Login implements Initializable {
    public TextField username;
    public TextField password;
    public FlowPane top;
    public Label tip;
    public Button loginBtn;
    public CheckBox remember;
    @Inject
    private Stage stage;
    @Inject
    private Provider<PersistService> persistServiceProvider;
    @Inject
    private ConfigBean configBean;
    @Inject
    private ThreadFactory threadFactory;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new DragListener(stage).enableDrag(top);

        if(configBean.remember()){
            username.setText(configBean.username());
            password.setText(configBean.password());
            remember.setSelected(true);
        }

        threadFactory.newThread(()->{
            try {
                persistServiceProvider.get().start();
                Platform.runLater(()->{
                    tip.setText("请输入用户名密码");
                    loginBtn.setDisable(false);
                });
            } catch (Exception e) {
                Platform.runLater(()->MessageDialog.exception(stage,"连接数据库失败",e));
            }
        }).start();
    }
    public void login(ActionEvent actionEvent) {
        if (!Validor.validNullOrEmpty(username, tip, "用户名不可为空") ||
                !Validor.validNullOrEmpty(password, tip, "密码不可为空")) {
            return;
        }

        Parent load = ControllerFxs.load(Layout.class);
        Scene value = new Scene(load);
        stage.setScene(value);
        stage.centerOnScreen();

        if (remember.isSelected()) {
            try {
                Properties properties = new Properties();
                @Cleanup InputStream fileInputStream = Files.newInputStream(ConfigBean.path);
                properties.load(fileInputStream);
                properties.setProperty("login.remember", "true");
                properties.setProperty("login.username", username.getText().trim());
                properties.setProperty("login.password", password.getText().trim());
                @Cleanup OutputStream fileOutputStream = Files.newOutputStream(ConfigBean.path);
                properties.store(fileOutputStream, LocalDateTime.now().toString());
            } catch (Exception e) {
                log.error("写入配置异常",e);
            }
        }
    }

    public void exit(ActionEvent actionEvent) {
        System.exit(1);
    }
}
