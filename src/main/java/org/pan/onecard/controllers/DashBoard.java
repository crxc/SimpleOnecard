package org.pan.onecard.controllers;

import lombok.extern.slf4j.Slf4j;
import org.pan.onecard.common.ControllerFx;

/**
 * @author panmingzhi
 */
@Slf4j
@ControllerFx(fxml = "/fxml/DashBoard.fxml")
public class DashBoard {
}
