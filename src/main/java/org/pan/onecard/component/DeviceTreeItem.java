package org.pan.onecard.component;

import javafx.collections.ObservableList;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import lombok.extern.slf4j.Slf4j;
import org.pan.onecard.bean.DeviceLink;
import org.pan.onecard.bean.DeviceTree;
import org.pan.onecard.common.EventBusHelper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author panmingzhi
 */
@Slf4j
public class DeviceTreeItem extends TreeItem<DeviceTree> {
    private boolean notInitialized = true;

    private DeviceTreeItem(DeviceTree value) {
        super(value);
    }

    public static void loadDataFromJson(TreeView<DeviceTree> tree, List<DeviceLink> deviceLinkList) {
        tree.setCellFactory(param -> new TreeCell<DeviceTree>() {
            @Override
            protected void updateItem(DeviceTree item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : item.getName());
            }
        });

        tree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> EventBusHelper.getDefault().post(newValue));

        DeviceTree value = new DeviceTree();
        List<DeviceTree> collect = deviceLinkList.stream().map(DeviceTree::new).collect(Collectors.toList());
        value.setDeviceTreeList(collect);
        tree.setRoot(new DeviceTreeItem(value));
    }

    @Override
    public ObservableList<TreeItem<DeviceTree>> getChildren() {
        log.debug("value {} getChildren", getValue());
        if (notInitialized) {
            notInitialized = false;
            List<DeviceTreeItem> collect = getValue().getChildren().parallelStream().map(DeviceTreeItem::new).collect(Collectors.toList());
            super.getChildren().addAll(collect);
        }
        return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
        boolean b = getValue() == null || getValue().getChildren().size() <= 0;
        log.debug("value {} is leaf {}", getValue(), b);
        return b;
    }
}
