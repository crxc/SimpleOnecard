package org.pan.onecard.component;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageDialog {

    public static boolean confirm(Stage stage, String headerText){
        log.info("confirm dialog:{}",headerText);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initOwner(stage);
        alert.setHeaderText(headerText);
        alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        return alert.showAndWait().filter(f->f == ButtonType.YES).isPresent();
    }

    public static void exception(Stage stage, String headerText,Throwable exception){
        log.error("exception dialog:{}",exception);
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(stage);
        alert.setHeaderText(headerText);
        alert.setContentText(exception.toString());
        alert.showAndWait();
    }
}
