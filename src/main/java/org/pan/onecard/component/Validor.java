package org.pan.onecard.component;

import com.google.common.base.Strings;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Validor {

    public static boolean validNullOrEmpty(TextField textField, Label tip, String msg){
        if(Strings.isNullOrEmpty(textField.getText().trim())){
            textField.requestFocus();
            tip.setText(msg);
            return false;
        }else{
            tip.setText("");
            return true;
        }
    }
}
