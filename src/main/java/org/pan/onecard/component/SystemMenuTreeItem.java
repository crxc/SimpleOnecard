package org.pan.onecard.component;

import com.alibaba.fastjson.JSON;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.pan.onecard.bean.SystemMenu;
import org.pan.onecard.common.EventBusHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author panmingzhi
 */
@Slf4j
public class SystemMenuTreeItem extends TreeItem<SystemMenu> {
    private boolean notInitialized = true;

    private SystemMenuTreeItem(SystemMenu value) {
        super(value);
    }

    public static void loadDataFromJson(TreeView<SystemMenu> tree, String jsonPath) {
        tree.setCellFactory(param -> new TreeCell<SystemMenu>() {
            @Override
            protected void updateItem(SystemMenu item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : item.getName());
            }
        });

        tree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> EventBusHelper.getDefault().post(newValue));

        try {
            @Cleanup InputStream inputStream = SystemMenuTreeItem.class.getResourceAsStream(jsonPath);
            String toString = IOUtils.toString(inputStream, "utf-8");
            List<SystemMenu> systemMenus = JSON.parseArray(toString, SystemMenu.class);
            tree.setRoot(new SystemMenuTreeItem(new SystemMenu("", "", "", systemMenus)));
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("加载菜单出错");
            alert.setContentText(e.toString());
            alert.showAndWait();
        }
    }

    @Override
    public ObservableList<TreeItem<SystemMenu>> getChildren() {
        log.debug("value {} getChildren", getValue());
        if (notInitialized) {
            notInitialized = false;
            List<SystemMenuTreeItem> collect = getValue().getChildren().parallelStream().map(SystemMenuTreeItem::new).collect(Collectors.toList());
            super.getChildren().addAll(collect);
        }
        return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
        boolean b = getValue() == null || getValue().getChildren().size() <= 0;
        log.debug("value {} is leaf {}", getValue(), b);
        return b;
    }
}
