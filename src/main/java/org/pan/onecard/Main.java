package org.pan.onecard;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import org.pan.onecard.common.ControllerFxs;
import org.pan.onecard.controllers.Login;

/**
 * @author panmingzhi
 */
@Slf4j
public class Main extends Application {
    public static void main(String[] args) {
        log.info("启动一卡通管理系统");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ControllerFxs.init(primaryStage);
        Parent load = ControllerFxs.load(Login.class);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("一卡通管理系统");
        Scene value = new Scene(load);
        primaryStage.setScene(value);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/image/logo.png")));
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("确认退出吗?");
            alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
            alert.showAndWait().filter(f -> f == ButtonType.YES).ifPresent(p -> System.exit(1));
            event.consume();
        });
    }
}
