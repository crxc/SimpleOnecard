package org.pan.onecard.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author panmingzhi
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DeviceLink extends BasicModel{
    private String code;
    private String name;
    @OneToMany
    private List<Device> deviceList;
}
