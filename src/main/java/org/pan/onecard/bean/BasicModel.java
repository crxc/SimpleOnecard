package org.pan.onecard.bean;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
public class BasicModel {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
}
