package org.pan.onecard.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * @author panmingzhi
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device extends BasicModel {
    private String code;
    private String name;
}
