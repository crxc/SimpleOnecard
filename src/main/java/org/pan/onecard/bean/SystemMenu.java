package org.pan.onecard.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author panmingzhi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemMenu {
    private String name;
    private String icon;
    private String controller;
    private List<SystemMenu> children = new ArrayList<>();
}
