package org.pan.onecard.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceTree {
    private Device device;
    private DeviceLink deviceLink;
    private List<DeviceTree> deviceTreeList = new ArrayList<>();

    public DeviceTree(Device device){
        this.device = device;
    }

    public DeviceTree(DeviceLink deviceLink){
        this.deviceLink = deviceLink;
    }

    public String getName() {
        if(device != null){
            return device.getName();
        }
        if (deviceLink != null) {
            return deviceLink.getName();
        }
        return null;
    }

    public Collection<DeviceTree> getChildren() {
        return null;
    }
}
